# Baconumeral: Data Engineering Challenge

## Application set-up

1. Add CSV files from the [dataset](https://www.kaggle.com/rounakbanik/the-movies-dataset) into the `./data/` directory
2. Run `npm run etl` to run an ETL workflow to populate the SQLite database
3. Run the application with `npm run start`
4. Post to `https://localhost:3000` with a JSON payload like:
    ```json
    {
      "actorOneName": "Norm Macdonald",
      "actorTwoName": "Sam Rockwell"
    }
    ```
5. Examine your Bacon Number in the resulting payload:
    ```json
    {
      "baconNumber": 2
    }
    ```
