import {createConnection} from 'typeorm';
import {findDistanceBetweenActors} from '../src/findDistanceBetweenActors';

const charlesSherlock = 'Charles Sherlock';
const fredAstaire = 'Fred Astaire';
const fredWard = 'Fred Ward';
const kevinBacon = 'Kevin Bacon';
const markHamill = 'Mark Hamill';

beforeAll(async () => {
  await createConnection();
});

test.each([
  [kevinBacon, kevinBacon, 0],
  [kevinBacon, fredWard, 1],
  [kevinBacon, fredAstaire, 2],
  [kevinBacon, charlesSherlock, 3],
  [markHamill, charlesSherlock, 3],
])(
  'Distance between %s and %s is %i',
  async (actorOneName, actorTwoName, expected) => {
    const distance = await findDistanceBetweenActors(
      actorOneName,
      actorTwoName
    );
    expect(distance).toBe(expected);
  }
);
