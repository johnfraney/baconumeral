import * as fs from 'fs';
import * as path from 'path';
import * as csv from 'fast-csv';
import {Connection, createConnection} from 'typeorm';
import {Movie} from './entity/Movie';
import {DATA_DIRECTORY} from './constants';
import {bulkInsertRecords} from './etlUtils';
import {MovieRow} from './types';

const MOVIES_METADATA_CSV_PATH = path.resolve(
  __dirname,
  '../',
  DATA_DIRECTORY,
  'movies_metadata.csv'
);

async function importMovies(connection: Connection) {
  console.log('Importing movies...');
  // Use a map to avoid duplicate entries
  const movies = new Map();

  fs.createReadStream(MOVIES_METADATA_CSV_PATH)
    .pipe(csv.parse({headers: true}))
    .on('error', error => console.error(error))
    .on('data', async row => {
      // Skip incomplete entries
      if (!row.id || !row.title || movies.has(row.id)) {
        return;
      }
      movies.set(row.id, row.title);
    })
    .on('end', async (rowCount: number) => {
      console.log(`Parsed ${rowCount} rows`);
      console.log(`Preparing to insert ${movies.size} movies`);

      // Insert multiple rows, but fewer than SQLite's default insert limit
      const movieDataList: MovieRow[] = [];
      movies.forEach((title, id) => movieDataList.push({id, title}));
      await bulkInsertRecords(connection, Movie, movieDataList, 200);
      const movieCount = await connection.manager.count(Movie);
      console.log(`Inserted ${movieCount} movies`);
    });
}

createConnection()
  .then(async connection => {
    await importMovies(connection);
  })
  .catch(error => console.log(error));
