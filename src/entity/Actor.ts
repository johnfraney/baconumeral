import {BaseEntity, Column, Entity, OneToMany, PrimaryColumn} from 'typeorm';
import {Credit} from './Credit';

@Entity()
export class Actor extends BaseEntity {
  @PrimaryColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => Credit, credit => credit.actor)
  credits: Credit[];
}
