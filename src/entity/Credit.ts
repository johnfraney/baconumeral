import {BaseEntity, Entity, ManyToOne, PrimaryColumn} from 'typeorm';
import {Actor} from './Actor';
import {Movie} from './Movie';

@Entity()
export class Credit extends BaseEntity {
  @PrimaryColumn()
  id: string;

  @ManyToOne(() => Actor, actor => actor.credits)
  actor: Actor;

  @ManyToOne(() => Movie, movie => movie.credits)
  movie: Movie;
}
