import {BaseEntity, Column, Entity, OneToMany, PrimaryColumn} from 'typeorm';
import {Credit} from './Credit';

@Entity()
export class Movie extends BaseEntity {
  @PrimaryColumn()
  id: string;

  @Column()
  title: string;

  @OneToMany(() => Credit, credit => credit.movie)
  credits: Credit[];
}
