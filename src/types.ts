// eslint-disable-next-line @typescript-eslint/ban-types
export type ORMEntity = string | Function | (new () => unknown);

export interface CreditData {
  id: number;
  credit_id: string;
  name: string;
}

export interface DataRecord {
  [key: string]: number | string | boolean;
}

export interface MovieRow {
  id: string;
  title: string;
}
