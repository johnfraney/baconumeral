import {getConnection} from 'typeorm';
import {chunk} from 'lodash';
import {CreditData, DataRecord, ORMEntity} from './types';

export async function bulkInsertRecords(
  recordType: ORMEntity,
  records: Array<DataRecord>,
  batchSize: number
): Promise<void> {
  const unsavedRecords: Set<DataRecord> = new Set();
  const connection = getConnection();
  for (const batch of chunk(records, batchSize)) {
    await connection
      .createQueryBuilder()
      .insert()
      .into(recordType)
      .values(batch)
      .execute()
      .catch(() => {
        batch.forEach(record => {
          unsavedRecords.add(record);
        });
      });
  }
  // Insert unsaved records individually so one failing foreign key does not spoil
  // the batch
  if (unsavedRecords.size === 0) {
    return;
  }
  console.info(`Attempting to insert ${unsavedRecords.size} unsaved records`);
  for (const record of unsavedRecords) {
    try {
      await connection.getRepository(recordType).insert(record);
      unsavedRecords.delete(record);
    } catch (error) {
      console.error(error);
    }
  }
  console.warn(`Could not save ${unsavedRecords.size} records`);
}

export function parseCreditColumn(creditColumn: string): CreditData[] {
  // Replace None with null for JS compatibility
  let jsCompatibleCreditColumn = creditColumn;
  while (jsCompatibleCreditColumn.includes(': None')) {
    jsCompatibleCreditColumn = jsCompatibleCreditColumn.replace(
      ': None',
      ': null'
    );
  }
  // Convert credit column to JS without using eval(). See:
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/eval#Never_use_eval!
  const credits = Function(
    '"use strict";return(' + jsCompatibleCreditColumn + ')'
  )();
  return credits;
}
