import {getConnection} from 'typeorm';
import {Actor} from './entity/Actor';

const ACTORS_IN_MOVIES_WITH_ACTOR_QUERY_TEMPLATE =
  'SELECT DISTINCT actorId FROM credit WHERE movieId IN (SELECT DISTINCT movieId FROM credit WHERE actorId in ({}))';

/**
 * Finds the "Bacon Number" between two actors.
 * Builds a tree structure where child nodes are actors who have appeared in a film
 * alongside the parent-node actor.
 */
export async function findDistanceBetweenActors(
  actorOneName: string,
  actorTwoName: string
): Promise<number | undefined> {
  console.info(
    `Calculating Bacon Number for ${actorOneName} and ${actorTwoName}`
  );
  let actorDistance = 0;
  let foundActorDistance = false;

  // Handle a Bacon Number of zero without hitting the database
  if (actorOneName === actorTwoName) {
    return actorDistance;
  }
  const actorOne = await Actor.findOneOrFail({name: actorOneName});
  const actorTwo = await Actor.findOneOrFail({name: actorTwoName});

  const connection = getConnection();

  // Keep track of started trees so they aren't duplicated in other levels
  const startedTrees: Set<number> = new Set();
  let actorIdHaystack = new Set([actorOne.id]);

  while (foundActorDistance === false) {
    const childNodeActorIds: Set<number> = new Set();
    // Update the haystack with actors in the next level of the tree, skipping
    // actors who already have a node
    const actorIdQueryString = Array.from(actorIdHaystack.values()).join(',');
    const newActorIds = await connection.query(
      ACTORS_IN_MOVIES_WITH_ACTOR_QUERY_TEMPLATE.replace(
        '{}',
        actorIdQueryString
      )
    );
    console.info(`Searching actors for a match: ${newActorIds.length}`);
    for (const newActorId of newActorIds) {
      if (actorTwo.id === newActorId.actorId) {
        foundActorDistance = true;
        break;
      }
      if (startedTrees.has(newActorId.actorId)) {
        continue;
      }
      childNodeActorIds.add(newActorId.actorId);
      startedTrees.add(newActorId.actorId);
    }
    actorDistance += 1;
    actorIdHaystack = childNodeActorIds;
  }
  console.info(`${actorTwo.name} found at a distance of ${actorDistance}`);
  return actorDistance;
}
