import {createConnection} from 'typeorm';
import express from 'express';
import {findDistanceBetweenActors} from './findDistanceBetweenActors';
import {PORT} from './constants';

const app = express();
app.use(express.json());

app.post('/', async (request, response) => {
  const {actorOneName, actorTwoName} = request.body;
  const baconNumber = await findDistanceBetweenActors(
    actorOneName,
    actorTwoName
  ).catch(() => null);
  return response.json({baconNumber});
});

createConnection()
  .then(() => {
    app.listen(PORT, () =>
      console.log(`Baconumeral listening at http://localhost:${PORT}`)
    );
  })
  .catch(error => console.log(error));
