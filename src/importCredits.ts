import * as fs from 'fs';
import * as path from 'path';
import * as csv from 'fast-csv';
import {createConnection, getConnection} from 'typeorm';
import {bulkInsertRecords, parseCreditColumn} from './etlUtils';

import {Actor} from './entity/Actor';
import {Credit} from './entity/Credit';

const CREDITS_CSV_PATH = path.resolve(__dirname, '../', 'data', 'credits.csv');

async function importActorsAndCredits() {
  console.log('Importing actors and credits...');
  // Use maps to prevent duplicate entries
  const actors = new Map();
  const credits = new Map();
  const movieIds = new Set();
  const connection = getConnection();

  fs.createReadStream(CREDITS_CSV_PATH)
    .pipe(csv.parse({headers: true}))
    .on('error', error => console.error(error))
    .on('data', row => {
      const movieId = row.id;

      // Skip incomplete rows
      if (!movieId || !row.cast) {
        return;
      }

      // Skip duplicate rows
      if (movieIds.has(movieId)) {
        console.info(`Skipping duplicate movie entry: ${movieId}`);
        return;
      }
      movieIds.add(movieId);

      const castCredits = parseCreditColumn(row.cast);
      castCredits.forEach(castCredit => {
        const actorId = castCredit.id;
        const creditId = castCredit.credit_id;
        if (!actors.has(actorId)) {
          actors.set(actorId, {id: actorId, name: castCredit.name});
        }
        if (!credits.has(creditId)) {
          credits.set(creditId, {
            actor: actorId,
            id: creditId,
            movie: movieId,
          });
        }
      });
    })
    .on('end', async (rowCount: number) => {
      console.log(`Parsed ${rowCount} rows`);
      await bulkInsertRecords(Actor, Array.from(actors.values()), 200);
      const actorCount = await connection.manager.count(Actor);
      console.log(`Inserted ${actorCount} actors`);

      await bulkInsertRecords(
        Credit,
        Array.from(new Set(credits.values())),
        200
      );
      const creditCount = await connection.manager.count(Credit);
      console.log(`Inserted ${creditCount} credits`);
    });
}

createConnection()
  .then(async () => {
    await importActorsAndCredits();
  })
  .catch(error => console.log(error));
